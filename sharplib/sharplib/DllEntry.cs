﻿using RGiesecke.DllExport;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Security.Cryptography;
using System.Net.NetworkInformation;
using Microsoft.Win32;
using System.Security.Principal;
using System.DirectoryServices;

namespace sharplib
{
    public class DllEntry
    {

        [DllExport("_RVExtensionVersion@8", CallingConvention = CallingConvention.Winapi)]
        public static void RvExtensionVersion(StringBuilder output, int outputSize)
        {
            output.Append("Cake v1.0");
        }

        [DllExport("_RVExtension@12", CallingConvention = CallingConvention.Winapi)]
        public static void RvExtension(StringBuilder output, int outputSize,
           [MarshalAs(UnmanagedType.LPStr)] string function)
        {
            switch (function)
            {
                case "hello":
                    output.Append("Hola, Debugger");
                    break;
                case "GetCPUId":
                    output.Append(GetCPUID());
                    break;
                case "GetCPUId.MD5":
                    output.Append(CreateMD5(GetCPUID()));
                    break;
                case "GetCPUId.SHA256":
                    output.Append(SHA256(GetCPUID()));
                    break;
                case "GetMotherID":
                    output.Append(GetMotherID());
                    break;
                case "GetMotherID.MD5":
                    output.Append(CreateMD5(GetMotherID()));
                    break;
                case "GetMotherID.SHA256":
                    output.Append(SHA256(GetMotherID()));
                    break;
                case "GetMacAddress":
                    output.Append(GetMacAddress().ToString());
                    break;
                case "GetProductID":
                    output.Append(GetProductID());
                    break;
                case "GetHDDId":
                    output.Append(GetHDDId());
                    break;
                case "GetComputerSid":
                    output.Append(GetComputerSid());
                    break;
            } 
        }
        public static SecurityIdentifier GetComputerSid()
        {
            return new SecurityIdentifier((byte[])new DirectoryEntry(string.Format("WinNT://{0},Computer", Environment.MachineName)).Children.Cast<DirectoryEntry>().First().InvokeGet("objectSID"), 0).AccountDomainSid;
        }
        public static string GetHDDId()
        {
            string hddInfo = string.Empty;
            ManagementObjectSearcher searcher =
         new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

            foreach (ManagementObject info in searcher.Get())
            {
                hddInfo = info["SerialNumber"].ToString();
            }
            return hddInfo;
        }
        public static string GetProductID()
        {
            RegistryKey localMachine = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey windowsNTKey = localMachine.OpenSubKey(@"Software\Microsoft\Windows NT\CurrentVersion");
            object productID = windowsNTKey.GetValue("ProductId");
            return productID.ToString();
        }
        public static string GetCPUID()
        {
            string cpuInfo = string.Empty;
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();

            foreach (ManagementObject mo in moc)
            {
                cpuInfo = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return cpuInfo;
        }
        public static string GetMotherID()
        {
            string motherInfo = string.Empty;
            ManagementObjectSearcher MOS = new ManagementObjectSearcher("Select * From Win32_BaseBoard");
            foreach (ManagementObject getserial in MOS.Get())
            {
                motherInfo = getserial["SerialNumber"].ToString();
            }
            return motherInfo;
        }
        public static PhysicalAddress GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    return nic.GetPhysicalAddress();
                }
            }
            return null;
        }
        public static string CreateMD5(string input)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        static string SHA256(string input)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(input));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }
    }
}
